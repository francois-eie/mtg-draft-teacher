import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { HardcodedValues } from './shared/hardcoded-values';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  arrAllCards = [];
  arrDisplayCards = [];

  arrBlackCards = [];
  arrBlueCards = [];
  arrGreenCards = [];
  arrRedCards = [];
  arrWhiteCards = [];
  
  constructor(
    private httpclient: HttpClient,
    private hardcodedvalues: HardcodedValues
  ) {}

  async ngOnInit() {
    await this.getCards('https://api.scryfall.com/cards/search?order=set&q=e%3Athb');
    console.log('arrAllCards', this.arrAllCards);
    this.splitCardsByColour();
    this.displayBestCards();
  }

  async getCards(pUrl) {
    // This function calls the card list API multiple times by looping through itself until all the card-pages have been loaded into this.arrAllCards
    await new Promise(resolve => {
      this.httpclient.get<any>(pUrl).subscribe(response => {
        this.arrAllCards = [...this.arrAllCards, ...response.data];
        (response.has_more) ? resolve(this.getCards(response.next_page)) : resolve();
      });
    });
  }

  splitCardsByColour() {
    // This splits all the cards by the 5 colours (it ignores multi colours cards and colorsless cards)
    this.arrAllCards.forEach(element => {
      if (element.colors.length != 1) return;

      switch (element.colors[0]) {
        case 'B':
          this.arrBlackCards.push(element);
          break;
        case 'G':
          this.arrGreenCards.push(element);
          break;
        case 'R':
          this.arrRedCards.push(element);
          break;
        case 'U':
          this.arrBlueCards.push(element);
          break;
        case 'W':
          this.arrWhiteCards.push(element);
          break;
      };
    });

    console.log('arrBlackCards', this.arrBlackCards);
    console.log('arrGreenCards', this.arrGreenCards);
    console.log('arrRedCards', this.arrRedCards);
    console.log('arrBlueCards', this.arrBlueCards);
    console.log('arrWhiteCards', this.arrWhiteCards);
  }

  displayBestCards() {
    this.arrDisplayCards = [];

    this.arrAllCards.forEach(element => {
      if (
        this.hardcodedvalues.bestBlackCommons.includes(element.name) ||
        this.hardcodedvalues.bestBlueCommons.includes(element.name) ||
        this.hardcodedvalues.bestGreenCommons.includes(element.name) ||
        this.hardcodedvalues.bestRedCommons.includes(element.name) ||
        this.hardcodedvalues.bestWhiteCommons.includes(element.name)
      ) this.arrDisplayCards.push(element);
    });

    console.log(this.arrDisplayCards);
  }
}
