import { Injectable } from '@angular/core';

@Injectable()
export class HardcodedValues {
  // LSV
  bestBlackCommons = ['Mire\'s Grasp', 'Final Death', 'Venomous Hierophant', 'Underworld Charger', 'Discordant Piper'];
  bestBlueCommons = ['Thirst for Meaning', 'Ichthyomorphosis', 'Omen of the Sea', 'Vexing Gull', 'Naiad of Hidden Coves'];
  bestGreenCommons = ['Warbriar Blessing', 'Ilysian Caryatid', 'Loathsome Chimera', 'Voracious Typhon', 'Setessan Training'];
  bestRedCommons = ['Iroas\'s Blessing', 'Omen of the Forge', 'Flummoxed Cyclops', 'Incendiary Oracle', 'Final Flare'];
  bestWhiteCommons = ['Dreadful Apathy', 'Daybreak Chimera', 'Heliod\'s Pilgrim', 'Triumphant Surge', 'Indomitable Will'];
}